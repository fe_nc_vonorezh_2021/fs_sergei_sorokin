const gulp = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const path = require('path')
const srcPath = path.resolve(__dirname, '../js/**/*.js')

gulp.task('js', function () {
    return gulp.src(srcPath)
        .pipe(concat('test.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'))
});