let btn = document.querySelector('button')

let game = function() {
    let count = 0
    let num = Math.floor(Math.random() * 1000) + 1
    let userNum = prompt("Введите число!") 
    while (num !== userNum) {
        if (isNaN(userNum) || userNum === '') {
            userNum = prompt("Введите корректное число!")
        }
        else if (num > userNum) {
            userNum = prompt("Искомое число больше!")
        }
        else if (num < userNum) { 
            userNum = prompt("Искомое число меньше!")
        }
        else if (num === +userNum) 
        {
            let replay = confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`)
            if (replay) {
                game()
            }
            else {
                return 
            }
        }
        count++
    }
}

btn.addEventListener('click', function() {
    game()
})