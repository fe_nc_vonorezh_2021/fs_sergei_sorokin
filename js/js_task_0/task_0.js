// Task 1
let a = 5
let b = 12

console.log(a, b)

let c = a 
a = b
b = c

console.log(a, b)

// Task 2

a = 3
b = 8

console.log(a, b);

[a, b] = [b, a]

console.log(a, b);