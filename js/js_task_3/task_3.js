const arr = Array.from({length: 10}, () => Math.floor(Math.random() * 200))
const arr2 = [2, 3, 4, 5, 6, 7]

const arrSort = function(arr, howSort) {
    if (howSort === 'asc') {
        arr.sort((a, b) => {
            return a - b
        })
        return arr
    }
    else if (howSort === 'desc') {
        arr.sort((a, b) => {
            return b - a
        })
        return arr
    }
}

console.log(arrSort(arr, 'asc'))
console.log(arrSort(arr, 'desc'))

const summSqOddEl = function(arr) {
    return arr.filter(n => n % 2).reduce((acc, cur) => acc + cur ** 2, 0);
}

console.log(summSqOddEl(arr2))