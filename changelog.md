# Changes
| Date | Title | Description | Link
| :---: | :---: | :---:| :---: |
| 14.10 | Task_1 | Added readme and changelog files | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests) |
| 19.10 | Task_2 | HTML and CSS done | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests) |
| 25.10 | Task_4 | js tasks completed | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests) |
| 03.11 | Task_5 | made a calculator with UI and a task with OOP | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/5) |
| 08.11 | Task_6 | localStorage and cookies | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/6) |
| 11.11 | Task_7 | table is done | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/7) |
| 16.11 | Task_8 | using gump and webpack | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/8) |
| 27.11 | Task_9 | using type script | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/9) |
| 27.11 | Task_9 | using type script | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/9) || 16.11 | Task_10 | rxjs and algorithms | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/8) |
| 16.11 | Task_11 | Angular and network | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/11) |
| 16.11 | Task_12 | Angular it's beautiful | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/12) |
| 16.11 | Task_13 | Ngrx and tests | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/15) |
| 27.11 | Task_14 | docker, nginx, express, mongo | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/14) |
| 27.11 | Task_15 | react | [MR](https://gitlab.com/fe_nc_vonorezh_2021/fs_sergei_sorokin/-/merge_requests/13) |

