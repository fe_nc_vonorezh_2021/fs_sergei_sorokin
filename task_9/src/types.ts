interface IBussines {
    name: string
    profit: number
    costs: number
    productionСosts(cost: number): void
}

interface NewProductBase<U> {
    productionCost: number
    visitorsSum: number
    profit: number
    name: U
    purchase(productCost: number): void
    businessInfo(): string
}

interface NewService<U> {
    name: U
    productionCost: number
    profit: number
    orderService(serviceCost: number): void
    businessInfo(): string
}

interface BusinessFactory<U> {
    createProductBase(name: U, productionCost: number): NewProductBase<U>

    createService(name: U, productionCost: number): NewService<U>
}

export {IBussines, NewProductBase, NewService, BusinessFactory}