import { NewProductBase, NewService } from "./types"

class ConcreteProductBase<U> implements NewProductBase<U> {
    productionCost: number
    visitorsSum: number
    profit: number
    name: U

    constructor (name: U, productionCost: number) {
        this.name = name
        this.productionCost = productionCost
        this.visitorsSum = 0
        this.profit = 0
    }

    purchase(productCost: number) {
        this.profit = this.profit + productCost - this.productionCost
        this.visitorsSum += 1
    }

    businessInfo() {
        return `Business name: ${this.name}, income: ${this.profit}, expenses: ${this.productionCost}, number of visitors: ${this.visitorsSum}`
    }
}

class ConcreteService<U> implements NewService<U> {
    name: U
    productionCost: number
    profit: number

    constructor (name: U, productionCost: number) {
        this.name = name
        this.productionCost = productionCost
        this.profit = 0
    }

    orderService(serviceCost: number) {
        this.profit =this.profit + serviceCost - this.productionCost
    }

    businessInfo() {
        return `Business name: ${this.name}, income: ${this.profit}, expenses: ${this.productionCost}`
    }
}

export {ConcreteProductBase, ConcreteService}