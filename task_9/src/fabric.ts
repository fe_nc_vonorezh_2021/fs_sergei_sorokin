import { BusinessFactory, NewProductBase, NewService, } from "./types"
import { ConcreteProductBase, ConcreteService } from "./models"

class ConcreteFactory1<U> implements BusinessFactory<U> {
    createProductBase(name: U, productionCost: number): NewProductBase<U> {
        return new ConcreteProductBase(name, productionCost)
    }

    createService(name: U, productionCost: number): NewService<U> {
        return new ConcreteService(name, productionCost)
    }
}
export {ConcreteFactory1}