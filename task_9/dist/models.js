class ConcreteProductBase {
    constructor(name, productionCost) {
        this.name = name;
        this.productionCost = productionCost;
        this.visitorsSum = 0;
        this.profit = 0;
    }
    purchase(productCost) {
        this.profit = this.profit + productCost - this.productionCost;
        this.visitorsSum += 1;
    }
    businessInfo() {
        return `Business name: ${this.name}, income: ${this.profit}, expenses: ${this.productionCost}, number of visitors: ${this.visitorsSum}`;
    }
}
class ConcreteService {
    constructor(name, productionCost) {
        this.name = name;
        this.productionCost = productionCost;
        this.profit = 0;
    }
    orderService(serviceCost) {
        this.profit = this.profit + serviceCost - this.productionCost;
    }
    businessInfo() {
        return `Business name: ${this.name}, income: ${this.profit}, expenses: ${this.productionCost}`;
    }
}
export { ConcreteProductBase, ConcreteService };
