import { ConcreteProductBase, ConcreteService } from "./models";
class ConcreteFactory1 {
    createProductBase(name, productionCost) {
        return new ConcreteProductBase(name, productionCost);
    }
    createService(name, productionCost) {
        return new ConcreteService(name, productionCost);
    }
}
export { ConcreteFactory1 };
