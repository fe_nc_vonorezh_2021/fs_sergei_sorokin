import React from "react";

export type ThemeType = {
    isDark: boolean;
    toggleTheme: () => void;
}

export default React.createContext<ThemeType>({
    isDark: false,
    toggleTheme: () => {}
})