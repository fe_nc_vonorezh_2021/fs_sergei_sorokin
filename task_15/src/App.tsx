import React, {useMemo, useState} from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import AnimalList from './components/ListAnimal/ListAnimal';
import Animal from "./components/Animal/Animal";
import ThemeContext from "./ThemeContext";
import Header from "./components/Header/Header";
import './App.css';

export const url = process.env.REACT_APP_BASE_URL || 'http://localhost:3000/animals';
export const themes = {
    light: {
        color: "#0A0909",
        background: "#F4F9F9",
        buttonColor: "#F4D9E9",
    },
    dark: {
        color: "#FFF6E3",
        background: "#1D2026",
        buttonColor: "#BF2C83",
    }
};

const App: React.FC = () => {
    const [isDark, setIsDark] = useState(false);

    const context = useMemo(() => ({
        isDark,
        toggleTheme: () => setIsDark(prev => !prev)
    }), [isDark]);

    const s = isDark ? themes.dark : themes.light;

    return (
        <BrowserRouter>
            <ThemeContext.Provider value={context}>
                <div className="container" style={{backgroundColor: s.background}}>
                    <Header/>
                    <Routes>
                        <Route path='/' element={<AnimalList/>}/>
                        <Route path='/animal/:id' element={<Animal/>}/>
                    </Routes>
                </div>
            </ThemeContext.Provider>
        </BrowserRouter>
    );
}

export default App;