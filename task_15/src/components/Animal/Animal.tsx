import React, {useContext, useEffect, useState} from "react";
import {useParams} from "react-router-dom";

import {themes, url} from "../../App";
import ThemeContext from "../../ThemeContext";
import "./Animal.css";

export type AnimalType = {
    name: string;
    type: string;
    color: string;
    age: number;
    breed: string;
    gender: string;
    id: number;
};

const Animal: React.FC = () => {
    const [animal, setAnimal] = useState<AnimalType | null>(null);
    const params = useParams()
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;
    
    useEffect(() => {
        fetch(`${url}/${params.id}`)
            .then(response => response.json())
            .then(data => setAnimal(data))
    },)

    return (
        <div className="info"
             style={{backgroundColor: themeStyle.background, color: themeStyle.color}}>
            {animal ?
                <div className="card" style={{width: "300px"}}>
                    <p>{animal.type} {animal.name}</p>
                    <p>Color: {animal.color}</p>
                    <p>Age: {animal.age}</p>
                    <p>Breed: {animal.breed}</p>
                    <p>Gender: {animal.gender}</p>
                </div>
                :
                <></>}
        </div>
    )
}

export default Animal;