import React, {useContext} from "react";
import {themes} from "../../App";

import  "./Button.css";
import ThemeContext from "../../ThemeContext";

type ButtonProps = {
    onClick: () => void,
    value: string
}

const Button: React.FC<ButtonProps> = props => {
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light

    return (
    <button className="cats-btn"
            style={{backgroundColor: themeStyle.buttonColor, color: themeStyle.color}}
            onClick={props.onClick}>{props.value}
    </button>
    );
};

export default React.memo(Button);