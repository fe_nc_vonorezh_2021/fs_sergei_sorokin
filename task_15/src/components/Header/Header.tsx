import React, {useContext} from "react";

import ThemeContext from "../../ThemeContext";
import {themes} from "../../App";
import "./Header.css";

const Header: React.FC = () => {
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;

    return (
        <header className="header" style={{backgroundColor: themeStyle.background}}>
            <button style={{backgroundColor: themeStyle.buttonColor, color: themeStyle.color}}
                    className="theme-btn"
                    onClick={() => context.toggleTheme()}>{context.isDark ? 'Light' : 'Dark'} theme
            </button>
            <h1 style={{color: themeStyle.color}}>Animal search!</h1>
        </header>
    );
}

export default Header;