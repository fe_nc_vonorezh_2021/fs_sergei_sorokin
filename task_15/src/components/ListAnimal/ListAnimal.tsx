import {useCallback, useContext, useEffect, useState} from "react";
import {NavLink} from "react-router-dom";

import {themes, url} from "../../App";
import ThemeContext from "../../ThemeContext";
import {AnimalType} from "../Animal/Animal";
import "./ListAnimal.css";
import Button from "../Button/Button";


const AnimalList = () => {
    const [animals, setAnimals] = useState<AnimalType[]>([]);
    const [isCatsShown, showCats] = useState<boolean>(true);
    const context = useContext(ThemeContext);
    const themeStyle = context.isDark ? themes.dark : themes.light;

    useEffect(() => {
        let cleanUp = false;

        fetch(url)
            .then(response => response.json())
            .then(data => !cleanUp ? setAnimals(data) : undefined)
            .catch(err => console.error(err));

        return () => {
            cleanUp = true;
        }
    }, []);
    const handleClick = useCallback(() => showCats(prevState => !prevState), []);

    return (
        <div className="animals" style={{backgroundColor: themeStyle.background,}}>
            <ul className="animals_ul" style={{display: "flex", justifyContent: "space-around"}}>{
                animals
                    .filter(item => isCatsShown ? true : item.type !== 'cat')
                    .map(animal =>
                        <li key={animal.id} className="card">
                            <NavLink to={`/animal/${animal.id}`} style={{color: themeStyle.color}}>
                                <div>
                                    <h4>{animal.type} {animal.name}</h4>
                                </div>
                            </NavLink>
                        </li>)}
            </ul>
            <Button onClick={handleClick}
                    value={isCatsShown ? "Delete cats" : "Show cats"} />
        </div>

    )
}

export default AnimalList;