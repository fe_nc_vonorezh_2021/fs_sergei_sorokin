const cells = document.querySelectorAll('tbody tr td')
const del = document.querySelector('.table__delete')
const edit = document.querySelector('.table__edit')
const add = document.querySelector('.table__add')
const table = document.querySelector('.table__tbody')

const tableRow = 
`<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
</tr>`

const deleteCell = (target) => {
    target.innerHTML = ''
}

const editCell = (target) => {
    target.innerHTML = prompt('Измените значение ячейки', target.textContent)
}

const addRow = () => {
    table.insertAdjacentHTML('beforeend', tableRow)
    const newCells = table.querySelectorAll(`tr:last-child`)
    console.log(newCells)
    for (const cell of newCells) {
        cell.addEventListener('click', handleCellClick)
    }
}

const handleCellClick = ({target}) => {
	if (del.classList.contains('active')) return deleteCell(target)
    if (edit.classList.contains('active')) return editCell(target)
}

for (const cell of cells) {
	cell.addEventListener('click', handleCellClick)
}

const handleEdit = () => {
	edit.classList.toggle('active')
    del.classList.remove('active')
}

const handleDel = () => {
	edit.classList.remove('active')
    del.classList.toggle('active')
}

const handleAddActive = () => {
    add.classList.add('active')
}

const handleDelActive = () => {
    add.classList.remove('active')
    addRow()
}

del.addEventListener('click', handleDel)
edit.addEventListener('click', handleEdit)
add.addEventListener('mousedown', handleAddActive)
add.addEventListener('mouseup', handleDelActive)
