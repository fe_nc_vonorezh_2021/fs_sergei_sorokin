db = db.getSiblingDB('animal_db');

db.createCollection('animals');

db.animals.insertMany([
    {
        "name": "mouse",
        "type": "Mike",
        "color": "white",
        "age": "1",
        "breed": "not",
        "gender": "male",
        "id": 7
      },
      {
        "name": "Joe",
        "type": "cat",
        "color": "black",
        "age": "1",
        "breed": "siamse",
        "gender": "female",
        "id": 8
      },
      {
        "name": "Backs",
        "type": "dog",
        "color": "white",
        "age": "2",
        "breed": "samoyed",
        "gender": "male",
        "id": 9
      }
]);