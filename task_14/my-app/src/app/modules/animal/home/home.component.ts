import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/app.component';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  cards!: Card[]
  toggle: boolean = true
  isOpenForm: boolean = false
  allCard!: Card[]

  constructor(private api: ApiService) {
   }

  ngOnInit(): void {
    this.getAllAnimal()
  }


  toggleCards():void {
    !this.toggle ? this.cards = this.allCard :
    this.cards = this.allCard.filter(card => card.type !== 'cat')
    this.toggle = !this.toggle
  }

  getAllAnimal() {
    this.api.getAnimal()
    .subscribe(res=> {
      this.cards = res
      this.allCard = res
    }) 
  }
  
  openForm() {
    this.isOpenForm = !this.isOpenForm
  }
}