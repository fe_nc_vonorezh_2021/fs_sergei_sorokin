import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionAnimalComponent } from './description-animal.component';

describe('DescriptionAnimalComponent', () => {
  let component: DescriptionAnimalComponent;
  let fixture: ComponentFixture<DescriptionAnimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescriptionAnimalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionAnimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
