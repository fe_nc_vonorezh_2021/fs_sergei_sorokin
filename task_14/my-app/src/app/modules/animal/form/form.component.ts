import { ApiService } from './../shared/api.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Card } from './../../../app.component';
import { HomeComponent } from '../home/home.component';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less']
})
export class FormComponent implements OnInit {

  constructor(private api: ApiService, private home: HomeComponent) {
  }

  @Input()
  card!: Card;

  createNewCard(): Card {
    return {
      name: '',
      type: '',
      color: '',
      age: 0,
      breed: '',
      gender: '',
      id: 0
    } 
  }

  newCard: Card = this.createNewCard()

  ngOnInit(): void {
  }

  @Output()
    addEvent = new EventEmitter<Card>();

  postAnimalDetails() {
    this.api.postAnimal(this.newCard)
    .subscribe({
      next: res => {
        console.log(res)
      },
      error: () => alert("Something went wrong"),
      complete: () => this.home.getAllAnimal()
    })

    this.newCard = this.createNewCard()
    this.home.isOpenForm = !this.home.isOpenForm
  }
}
