import { HomeComponent } from './modules/animal/home/home.component';
import { Card } from 'src/app/app.component';
import { EditAnimalComponent } from './modules/animal/edit-animal/edit-animal.component';
import { NgModule } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AnimalModule } from './modules/animal/animal.module';
import { AppComponent} from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AnimalModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [EditAnimalComponent, HomeComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
