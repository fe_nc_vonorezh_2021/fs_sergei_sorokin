import {ActionReducerMap} from "@ngrx/store";
import {AppState} from "../app-state/app.state";
import { explorerReducer } from "./card.reducers";

export const appReducers: ActionReducerMap<AppState, any> = {
  cards: explorerReducer
};
