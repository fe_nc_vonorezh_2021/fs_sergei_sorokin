import { createReducer, on } from "@ngrx/store";
import { Card } from "src/app/app.component";
import { CardActions, CardActionsType } from "../actions/card.actions";

export interface CardsState {
    list: Card[];
}
  
export const initialState: CardsState = {
    list: [],
};
  
export const explorerReducer = (
    state = initialState,
    action: CardActionsType,
): CardsState => {
  switch (action.type) {
    case CardActions.GetCardsSucces:
      return {
        ...state,
        list: action.payload,
      };
    default:
      return state
  }
}