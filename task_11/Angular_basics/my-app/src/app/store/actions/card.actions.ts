import { Action } from "@ngrx/store";
import { Card } from "src/app/app.component";


export enum CardActions {
    GetCards = '[Card] Get Cards',
    GetCardsSucces = '[Card] Get Card Succces'

}

export class GetCards implements Action {
  public readonly type = CardActions.GetCards;
}

export class GetCardsSuccess implements Action {
    public readonly  type = CardActions.GetCardsSucces;
    constructor(public payload: Card[]) { }
}
  
export type CardActionsType = GetCards | GetCardsSuccess
