import { CardsState, initialState } from "../reducers/card.reducers";

export interface AppState {
  cards: CardsState
}

export const initialAppState: AppState = {
  cards: initialState
};

export function getInitialState(): AppState {
  return initialAppState
}
