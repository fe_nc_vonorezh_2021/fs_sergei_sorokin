import {ApiService} from '../../modules/animal/shared/api.service';
import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {CardActions, GetCards, GetCardsSuccess} from "../actions/card.actions";
import {catchError, map, of, switchMap, tap} from "rxjs";
import { Card } from "src/app/app.component";

/*getAnimal() {
    return this.http.get<Card[]>("http://localhost:3000/animals")
      .pipe(map((res: Card[]) => {
        return res
      }))
  }
*/
    /*this.api.getAnimal()
    .subscribe(res=> {
      this.cards = res
      this.allCard = res
    })*/
    
@Injectable()
export class CardEffects {
    constructor(
        private api: ApiService,
        private actions$: Actions,
    ){}

  getAnimals = createEffect(() => {
    return this.actions$.pipe(
      ofType<GetCards>(CardActions.GetCards),
      switchMap(() => this.api.getAnimal()),
      switchMap((cardResponse: Card[]) => of(new GetCardsSuccess(cardResponse))),
      );
  });
}
