import { Component } from '@angular/core';
export class Card {
  name: string = ''
  type:string =  ''
  color: string = ''
  age: number = 0
  breed: string = ''
  gender: string = ''
  id: number = 0

  constructor(name: string, type: string, color: string, age: number, breed: string, gender: string, id: number) {
    this.name = name
    this.type = type
    this.color = color
    this.age = age
    this.breed = breed
    this.gender = gender
    this.id = id
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})

export class AppComponent {

}
