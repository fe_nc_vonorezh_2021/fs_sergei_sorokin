import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './modules/animal/home/home.component';
import { EditAnimalComponent } from './modules/animal/edit-animal/edit-animal.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'pet/:id', component: EditAnimalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
