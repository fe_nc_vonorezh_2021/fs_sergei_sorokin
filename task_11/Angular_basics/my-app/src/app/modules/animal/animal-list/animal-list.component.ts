import { Component, Input, OnInit } from '@angular/core';
import { Card } from './../../../app.component';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.less']
})
export class AnimalListComponent implements OnInit {
  isOpen: boolean = false

  @Input()
  card!: Card;

  ngOnInit(): void {
  }

}
