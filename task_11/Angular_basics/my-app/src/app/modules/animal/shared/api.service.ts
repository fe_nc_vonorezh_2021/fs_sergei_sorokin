import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { map } from 'rxjs/operators'
import { Card } from 'src/app/app.component';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  postAnimal(data: Card) {
    return this.http.post<Card>("http://localhost:3000/animals", data)
    .pipe(map((res: Card)=> {
      return res
    }))
  }

  getAnimal() {
    return this.http.get<Card[]>("http://localhost:3000/animals")
      .pipe(map((res: Card[]) => {
        return res
      }))
  }

  deleteAnimal(id: number) {
    return this.http.delete<Card>("http://localhost:3000/animals/" + id)
      .pipe(map((res: Card) => {
        return res;
      }))
  }
  
  editAnimal(data: Card, id: number) {
    return this.http.put<any>("http://localhost:3000/animals/"+id, data)
    .pipe(map((res: Card)=> {
      return res
    }))
  }
}
