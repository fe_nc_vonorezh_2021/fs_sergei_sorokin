import { GetCards } from 'src/app/store/actions/card.actions';
import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/app.component';
import { ApiService } from '../shared/api.service';
import { Store } from '@ngrx/store'
import { AppState } from 'src/app/store/app-state/app.state';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  cards!: Card[]
  toggle: boolean = true
  isOpenForm: boolean = false
  allCard!: Card[]

  constructor(
    private api: ApiService,
    private _store: Store<AppState>,
    ) {}

  ngOnInit(): void {
    this.getAllAnimal()
  }

  toggleCards():void {
    !this.toggle ? this.cards = this.allCard :
    this.cards = this.allCard.filter(card => card.type !== 'cat')
    this.toggle = !this.toggle
  }

  getAllAnimal() {
    this._store.dispatch(new GetCards());
    this._store.select('cards').subscribe(cards => {
      this.cards = cards.list
      this.allCard = cards.list
    })
  }
  
  openForm() {
    this.isOpenForm = !this.isOpenForm
  }
}
