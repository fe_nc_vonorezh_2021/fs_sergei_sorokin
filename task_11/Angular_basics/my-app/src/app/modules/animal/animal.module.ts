import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimalListComponent } from './animal-list/animal-list.component';
import { DescriptionAnimalComponent } from './description-animal/description-animal.component';
import { FormComponent } from './form/form.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { EditAnimalComponent } from './edit-animal/edit-animal.component';

@NgModule({
  declarations: [
    AnimalListComponent,
    DescriptionAnimalComponent,
    FormComponent,
    HomeComponent,
    EditAnimalComponent,
  ],
  exports: [
    AnimalListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
  ],
  providers: [
    DescriptionAnimalComponent
  ]
})
export class AnimalModule { }
