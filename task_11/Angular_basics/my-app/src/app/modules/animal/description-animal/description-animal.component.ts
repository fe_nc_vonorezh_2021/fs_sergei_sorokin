import { EditAnimalComponent } from './../edit-animal/edit-animal.component';
import { Component, Input, OnInit } from '@angular/core';
import { Card } from './../../../app.component';
import { ApiService } from '../shared/api.service';
import { HomeComponent } from '../home/home.component';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-description-animal',
  templateUrl: './description-animal.component.html',
  styleUrls: ['./description-animal.component.less']
})
export class DescriptionAnimalComponent implements OnInit {
  constructor(private api: ApiService, private home: HomeComponent, private editComp: EditAnimalComponent) {
  }

  ngOnInit(): void {
  }

  @Input()
  card!: Card;

  deleteAnimal(id: number) {
    this.api.deleteAnimal(id)
    .subscribe(res=> {
      this.home.getAllAnimal()
    })
  }

  edit(card: Card) {
    this.editComp.onEdit(card)
  }
}
