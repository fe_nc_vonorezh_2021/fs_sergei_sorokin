import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAnimalComponent } from './edit-animal.component';

describe('EditAnimalComponent', () => {
  let component: EditAnimalComponent;
  let fixture: ComponentFixture<EditAnimalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAnimalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAnimalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create form with 7 controls', () => {
    component.ngOnInit()
    expect(component.formValue.contains('name')).toBeTruthy()
    expect(component.formValue.contains('type')).toBeTruthy()
    expect(component.formValue.contains('age')).toBeTruthy()
    expect(component.formValue.contains('color')).toBeTruthy()
    expect(component.formValue.contains('breed')).toBeTruthy()
    expect(component.formValue.contains('gender')).toBeTruthy()
})

  it('should mark TYPE as invalid if value is EMPTY', () => {
    component.ngOnInit()
    const control = component.formValue.get('type')
    control?.setValue('')
    expect(control?.valid).toBeFalsy()
  })

  it('should mark NAME as invalid if value is EMPTY', () => {
    component.ngOnInit()
    const control = component.formValue.get('name')
    control?.setValue('')
    expect(control?.valid).toBeFalsy()
})

  it('should mark TYPE as invalid if value LENGTH LESS than 3', () => {
    component.ngOnInit()
    const control = component.formValue.get('type')
    control?.setValue('')
    expect(control?.valid).toBeFalsy()
})

  it('should mark NAME as invalid if value LENGTH LESS than 3', () => {
    component.ngOnInit()
    const control = component.formValue.get('type')
    control?.setValue('')
    expect(control?.valid).toBeFalsy()
  })
});
