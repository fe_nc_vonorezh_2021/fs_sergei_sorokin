import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Card } from 'src/app/app.component';
import { ApiService } from '../shared/api.service';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-edit-animal',
  templateUrl: './edit-animal.component.html',
  styleUrls: ['./edit-animal.component.less']
})
export class EditAnimalComponent implements OnInit {
  animal: Card = this.createNewCard()
  formValue = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
    type: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
    age: new FormControl(null, [Validators.required, Validators.minLength(1), Validators.maxLength(2)]),
    color: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
    breed: new FormControl(null, [Validators.required, Validators.minLength(3), Validators.maxLength(10)]),
    gender: new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(6)]),
  })
  constructor(private route: ActivatedRoute, public formbuilder: FormBuilder, private api: ApiService, private home: HomeComponent) {
  }

  createNewCard(): Card {
    return {
      name: '',
      type: '',
      color: '',
      age: 0,
      breed: '',
      gender: '',
      id: this.route.snapshot.params['id']
    } 
  }

  ngOnInit(): void {
  }

  onEdit(card: Card) {
    this.animal.name = card.name
    this.formValue.setValue({
      name: card.name,
      type: card.type,
      color: card.color,
      age: card.age,
      breed: card.breed,
      gender: card.gender
    })
  }

  updateAnimal() {
    this.animal.name = this.formValue.value.name
    this.animal.type = this.formValue.value.type
    this.animal.age = this.formValue.value.age
    this.animal.color = this.formValue.value.color
    this.animal.breed = this.formValue.value.breed
    this.animal.gender = this.formValue.value.gender

    this.api.editAnimal(this.animal, this.animal.id)
    .subscribe(res=> {
      this.formValue.reset()
      this.home.getAllAnimal()
    })
    this.animal = this.createNewCard()
  }
}
