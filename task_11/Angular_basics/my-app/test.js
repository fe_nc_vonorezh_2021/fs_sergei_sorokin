import { Selector } from 'testcafe'

fixture('Test').page('http://localhost:4200')

test('filter-cats-test', async t => {
    const animals = Selector('app-animal-list')
    await t
        .expect(animals.count).eql(3)
        .click("#cats-button")
        .expect(animals.count).eql(2)
        .click("#cats-button")
        .expect(animals.count).eql(3)
})

test('create-animal-test', async t => {
    await t
        .click('#create-animal')
        .typeText('#type-input', 'Test type')
        .typeText('#name-input', 'Test name')
        .typeText('#breed-input', 'Test breed')
        .typeText('#color-input', 'Test color')
        .typeText('#gender-input', 'Test gender')
        .typeText('#age-input', '2')
        .click('#send-button')
    Selector('.main-text').withText('Test type')
})

test('delete-animal-test', async t => {
    const openDescription = Selector('#main-desc').nth(-1)
    const deleteButton = Selector('#delete-button').nth(-1)
    await t
        .click(openDescription)
        .click(deleteButton)
})