const inputField = document.querySelector("input")
const btn = document.querySelector("button")
const weatherBlock = document.querySelector(".weather-block")
const imgBlock = document.querySelector(".img-block")

async function sendRequest(method, url) {
    const response = await fetch(url)
    return await response.json()
}

btn.onclick = () => sendRequest('GET', `https://api.openweathermap.org/data/2.5/weather?q=${inputField.value}&appid=11b0170256c69f9dfb240ee028cbddf9`)
    .then(data => {
        const weatherList = []
        const img = document.createElement('img')
        const date = (String(new Date())).split(' ').slice(1, 5).join(' ')
        console.log(date)
        weatherBlock.innerHTML = ''
        imgBlock.innerHTML = ''
        weatherList.push('Name of the city: ' + data.name)
        weatherList.push('Degrees Celsius: ' + Math.round(+data.main.temp - 273))
        weatherList.push('Feels like: ' + Math.round(+data.main.feels_like - 273))
        weatherList.push('Weather: ' + data.weather[0].main)
        weatherList.push('Description: ' + data.weather[0].description)
        weatherList.push('Wind speed: ' + data.wind.speed)
        img.src = `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`
        imgBlock.appendChild(img)
        for (let el of weatherList) {
            const p = document.createElement('p')
            p.textContent = el
            weatherBlock.appendChild(p)
        }
        const p = document.createElement('p')
        p.textContent = "Time of the last request: " + date
        weatherBlock.appendChild(p)
    })
    .catch(err => console.log(err))