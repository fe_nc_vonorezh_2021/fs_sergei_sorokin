"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.quickSort = void 0;
function quickSort(arr, left, right) {
    if (arr.length > 1) {
        let p = partion(arr, left, right);
        if (left < p - 1) {
            quickSort(arr, left, p - 1);
        }
        if (p < right) {
            quickSort(arr, p, right);
        }
    }
    return arr;
}
exports.quickSort = quickSort;
function partion(arr, left, right) {
    let pivot = Math.floor((left + right) / 2);
    while (left < right) {
        while (arr[left] < arr[pivot]) {
            left++;
        }
        while (arr[right] > arr[pivot]) {
            right--;
        }
        if (left <= right) {
            [arr[left], arr[right]] = [arr[right], arr[left]];
            left++;
            right--;
        }
    }
    return left;
}
