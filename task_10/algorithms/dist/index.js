"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const doublyLinkedList_1 = require("./doublyLinkedList");
const list = new doublyLinkedList_1.LinkedList();
list.addToBegin(1);
list.addToEnd(2);
list.addToEnd(3);
list.addToBegin(0);
list.addToEnd(11);
list.addToEnd(36);
list.addByIndex(9, 8);
list.remove(36);
list.addByIndex(1, 1);
list.edit(2, 66);
console.log(list.getElementByIndex(7));
console.log(list.getList());
