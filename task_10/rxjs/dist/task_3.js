"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const body = document.querySelector('body');
const getRandColor = () => {
    let first = Math.floor(Math.random() * 255);
    let second = Math.floor(Math.random() * 255);
    let third = Math.floor(Math.random() * 255);
    let str = 'rgb(' + first + ', ' + second + ', ' + third + ')';
    return str;
};
const btn1 = document.querySelector('.first-button');
const btn2 = document.querySelector('.second-button');
const btn3 = document.querySelector('.third-button');
const firstStream$ = (0, rxjs_1.fromEvent)(btn1, 'click');
const secondStream$ = (0, rxjs_1.fromEvent)(btn2, 'click');
const thirdStream$ = (0, rxjs_1.fromEvent)(btn3, 'click');
if (body != null) {
    (0, rxjs_1.merge)(firstStream$, secondStream$, thirdStream$).subscribe(value => body.style.background = getRandColor());
}
