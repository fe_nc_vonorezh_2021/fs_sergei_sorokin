"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const stream$ = (0, rxjs_1.range)(1, 100)
    .pipe((0, operators_1.filter)((v) => {
    if (v === 1) {
        return false;
    }
    else {
        for (let i = 2; i <= v; i++) {
            if (v % i === 0) {
                return false;
            }
        }
        return true;
    }
}));
stream$.subscribe((v) => {
    console.log(v);
});
