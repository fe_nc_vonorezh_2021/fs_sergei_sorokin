import { fromEvent, merge } from "rxjs"

const body: HTMLElement | null = document.querySelector('body')
const getRandColor = (): string => {
    let first: number = Math.floor(Math.random() * 255);
    let second: number = Math.floor(Math.random() * 255);
    let third: number = Math.floor(Math.random() * 255);
    let str: string = 'rgb(' + first + ', ' + second + ', ' + third + ')';
    return str;
};

const btn1: any = document.querySelector('.first-button');
const btn2: any = document.querySelector('.second-button');
const btn3: any = document.querySelector('.third-button');
const firstStream$ = fromEvent(btn1, 'click');
const secondStream$ = fromEvent(btn2, 'click');
const thirdStream$ = fromEvent(btn3, 'click');

if (body != null) {
    merge(firstStream$, secondStream$, thirdStream$).subscribe(value => body.style.background = getRandColor());
    }
