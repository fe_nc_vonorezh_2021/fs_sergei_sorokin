import {Observable} from 'rxjs';

const stream$ = new Observable(observer => {
    setTimeout(() => observer.next('5'), 1000)
    setTimeout(() => observer.next('4'), 2000)
    setTimeout(() => observer.next('3'), 3000)
    setTimeout(() => observer.next('2'), 4000)
    setTimeout(() => observer.next('1'), 5000)
    setTimeout(() => observer.error('Something went wrong'), 6000)
})

stream$.subscribe(
    (v) => {console.log(v)},
    (err) => {console.log(err)}
)
