import {range, Observable} from "rxjs"
import {filter} from "rxjs/operators"

const stream$: Observable<number> = range(1, 100)
.pipe(
    filter((v: number) => {
        if (v === 1) {
            return false
        } else {
            for(let i = 2; i <= v; i++) {
                if(v % i === 0) {
                    return false
                }
            }
            return true
        }
    })
)

stream$.subscribe(
(v: number) => {
    console.log(v)
}
);


