class Business {
    constructor(name) {
        this.name = name
        this.profit = 0
        this.costs = 0
    }

    productionСosts(cost) {
        this.profit -= cost
    }
}

class Service extends Business {
    constructor(name) {
        super(name)
        this.costs = 300
    }

    orderService() {
        this.profit += 1000
        super.productionСosts(this.costs)
    }
}

class Carsharing extends Service {
    constructor(name) {
        super(name)
        this.carList = ['Mercedes', 'Bmw', 'Toyota']
        this.cost = 400
    }

    checkCar() {
        return this.carList
    }

    orderService() {
        super.orderService()
        alert('You rent a car')
    }
}

class FoodDelivery extends Service {
    constructor(name) {
        super(name)
        this.time = 20
        this.costs = 50
        this.orderPlace = ['supermarket', 'McDonalds', 'BurgerKing', 'KFC'] 
    }

    get possiblePlaceToOrder() {
        return `List of places to order: ${this.orderPlace.join(', ')}`
    }

    set possiblePlaceToOrder(newRestaurant) {
        this.orderPlace.push(newRestaurant)
    }

    deliverySpeed() {
        return `Delivery speed ${this.time} minutes`
    }

    orderService() {
        super.orderService()
        this.deliverySpeed()
    }
}

class Products extends Business {
    constructor(name) {
        super(name)
        this.cost = 0
        this.visitorsSum = 0
    }

    sellProduct(costGoods = 600) {
        this.profit += costGoods
        this.visitorsSum += 1
        super.productionСosts(this.costs)
    }
}


class Supermarkets extends Products {
    constructor(name) {
        super(name)
        this.cost = 100
        this.numOfProducts = 500
    }

    sellProduct(costGoods) {
        super.sellProduct(costGoods)
        if (this.numOfProducts === 0) {
            return 'Sorry, the product is over!'
        }
        else {
            this.numOfProducts -= 1
        }
    }

    replenishAssortment() {
        this.numOfProducts += 500
    }
}

// deleting / changing / creating 
const delivery = new FoodDelivery('deliveryClub')

delete delivery.costs
console.log(delivery.costs)
delivery.costs = 200
console.log(delivery.costs)
delivery.costs = 250
console.log(delivery.costs)