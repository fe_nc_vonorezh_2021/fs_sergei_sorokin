const buttons = document.querySelectorAll('button')
const input = document.querySelector('.input')
const operations = ['+', '-', '*', '^', '/', '√', '=']
const separators = ['\\\+', '-','\\*', '/', '√', '\\\^'];
let clearInput = false

// Пустое поле заполняем 0
const setZero = function() {
    if (input.value === '') {
        input.value = '0'
    }
}

// Чистим инпут 
const resetInput = function() {
    input.value = '0'
}

// Удаляем ноль при вводе другого числа
const delZero = function() {
    if (input.value === '0') {
        input.value = ''
    }
}

const isFloat = function(value) {
    if (value.includes('.')) {
        return true
    }
}


// Парсим инпут в число
const parseFloatOrNum = function(firstNum, secondNum = '') {
    if (isFloat(firstNum)) {
        firstNum = parseFloat(firstNum)
    }
    else {
        firstNum = +firstNum
    }
    if (isFloat(secondNum)) { 
        secondNum = parseFloat(secondNum)
    }
    else {
        secondNum = +secondNum
    }
    return [firstNum, secondNum]
}

// Удаляем последний символ
const cancelCalculating = function() {
    const lastChar = input.value.slice(-1)
    if (lastChar === '+' || lastChar === '-' || lastChar === '/' 
    || lastChar === '*' || lastChar === '^' || lastChar === '√') {
        return ''
    }
    else {
        input.value = input.value.slice(0, -1)
    }
    setZero()
}

// Сравниваем символ с символом операции. false при наличии символа операции
const checkOperations = function(char) {
    for (const operation of operations) {
        if (char.includes(operation)) {
            return false
        }
    }
    return true
}

// Блокируем ввод запятой или
const blockInputEmptyAndEqual = function(btnValue) {
    if (input.value === '') {
        function isEmpty() {
            if (btnValue === '') {
                return true
            }
            else {
                return false
            }  
        }
        return isEmpty() 
    }
    else if (input.value === '0') {
        function isEqual() {
            if (btnValue === '=') {
                return true
            }
            else {
                return false
            }
        }
        return isEqual()
    }
    return false
}

const haveComma = function() {
    if (checkOperations(input.value)) {
        if (input.value.includes('.')) {
            return false 
        }
        else {
            return true
        }
    }
    else {
        const arr = input.value.split(new RegExp(separators.join('|'), 'g'))
        const lastNumber = arr[1]
        if (lastNumber.includes('.')) {
            return false
        }
        else if (lastNumber === '') {
            return false
        }
        else {
            return true
        }
    }
}

const putComma = function (value) {
    if (value === '.') {
        if (haveComma()) {
            input.value += value
        }
    }
    else {
        input.value += value 
    }
}

const add = function() {
    const numArr = input.value.split('+')
    input.value = numArr.reduce((total, amount) => {
        const newArr = parseFloatOrNum(total, amount)
        return newArr[0] + newArr[1]
    })
    clearInput = true
    
}

const substract = function() {
    const numArr = input.value.split('-')
    input.value = numArr.reduce((total, amount) => {
        const newArr = parseFloatOrNum(total, amount)
        return newArr[0] - newArr[1]
    })
    clearInput = true
}

const multiply = function() {
    const numArr = input.value.split('*')
    input.value = numArr.reduce((total, amount) => {
        const newArr = parseFloatOrNum(total, amount)
        return newArr[0] * newArr[1]
    })
    clearInput = true
}

const divide = function() {
    const numArr = input.value.split('/')
    input.value = numArr.reduce((total, amount) => {
        const newArr = parseFloatOrNum(total, amount)
        return newArr[0] / newArr[1]
    })
    clearInput = true
}

const square = function() {
    const numArr = input.value.split('^')
    input.value = numArr.reduce((total, amount) => {
        const newArr = parseFloatOrNum(total, amount)
        return newArr[0] ** newArr[1]
    })
    clearInput = true
}

const root = function() {
    const numArr = input.value.split('√')
    input.value = Math.sqrt(+numArr[0])
    let clearInput = true
}

const equal = function() {
    if (input.value.includes('+')) {
        add()
    }
    else if (input.value.includes('-')) {
        substract()
    }
    else if (input.value.includes('*')) {
        multiply()
    }
    else if (input.value.includes('/')) {
        divide()
    }
    else if (input.value.includes('^')) {
        square()
    }
    else if (input.value.includes('√')) {
        root()
    }    
}

for (let btn of buttons) {
    btn.addEventListener('click', () => {
        if (btn.value !== 'c' && btn.value !== 'del') {
            // Удаляем ноль стоящий по умолчанию, если введена не операция
            if (!btn.classList.contains('operation')) {
                delZero()
            }
            // Недопускаем ввод двух операций 
            if (checkOperations(btn.value) || checkOperations(input.value)) {
                // Блокируем введение пустого символа или равно первым значением
                if (blockInputEmptyAndEqual(btn.value)) {
                    input.value = '0'
                }
                // Добавляем значение на экран
                else {
                    if (!(btn.value === '=')) {
                        // Чистим поле при новом примере
                        if (clearInput) {
                            input.value = ''
                            clearInput = false
                            if (!checkOperations(btn.value) || (btn.value === '.')) {
                                input.value = 0
                            }
                            else {
                                putComma(btn.value)
                            }
                        }
                        else {
                            putComma(btn.value)
                        }
                    }
                }
            }
        }
        else if (btn.value === 'c') {
            resetInput()
        }
        else if (btn.value === 'del') {
            cancelCalculating()
        }
        if (btn.value === '√') {
            if(checkOperations()) {
                root()
            }
        }
        else if (btn.value === '=') {
            equal()
        }
    })
}

setZero()