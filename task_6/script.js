const form = document.querySelector('form')
const inputFields = document.querySelectorAll('input')
const submit = form.querySelector('input[type="submit"]')
const userName = document.querySelector('#name')
const surname = document.querySelector('#surname')
const email = document.querySelector('#email')
const phone = document.querySelector('#phone')
const textarea = form.querySelector('textarea')
const checkbox = form.querySelector('input[type="checkbox"]')

const isEmpty = /^\s*$/
const isMailCorrect = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i
const isPhoneCorrect = /^\+[\d]{1}\([\d]{3}\)[\d]{2}-[\d]{2}-[\d]{3}$/
const userData = {
    name: '',
    surname: '',
    email: '',
    phone: '',
    comment: ''
}

checkbox.addEventListener('click', () => {
    if (checkbox.checked) {
        submit.removeAttribute('disabled')
    }
    else {
        submit.setAttribute('disabled', 'disabled')
    }
})

submit.addEventListener('click', (event) => {
    event.preventDefault()
    const wrongFields = []
    if (isEmpty.test(userName.value)) {
        wrongFields.push(userName.id)
    }
    if (isEmpty.test(surname.value)) {
        wrongFields.push(surname.id)
    }
    if (!isMailCorrect.test(email.value)) {
        wrongFields.push(email.id)
    }
    if (!isEmpty.test(phone.value)) {
        if (!isPhoneCorrect.test(phone.value)) {
            wrongFields.push(phone.id)
        }
    }
    if (isEmpty.test(textarea.value)) {
        wrongFields.push(textarea.id)
    }
    if (!wrongFields.length) {
        userData.name = userName.value
        userData.surname = surname.value
        userData.email = email.value
        userData.phone = phone.value
        userData.comment = textarea.value
        const cookie = document.cookie
        if (cookie.includes(`${userName.value + surname.value}`)) {
            alert(`${userName.value + ' ' + surname.value}, ваше обращение обрабатывается!`)
        }
        else {
            alert(`${userName.value + ' ' + surname.value}, спасибо за обращение!`)
            document.cookie = `send=${userName.value + surname.value}`
        }
    }
    else {
        alert(`Поля ${wrongFields.join(', ')} заполнены не верно, пожалуйста исправьте.`)
    }
})


// localstorage

userName.value = localStorage.getItem('name');
userName.oninput = () => {
  localStorage.setItem('name', userName.value)
};

surname.value = localStorage.getItem('surname');
surname.oninput = () => {
  localStorage.setItem('surname', surname.value)
};

email.value = localStorage.getItem('email');
email.oninput = () => {
  localStorage.setItem('email', email.value)
};

phone.value = localStorage.getItem('phone');
phone.oninput = () => {
    localStorage.setItem('phone', phone.value)
};

textarea.value = localStorage.getItem('comment');
textarea.oninput = () => {
    localStorage.setItem('comment', textarea.value)
};
